import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart'; 


IconData getIcon(String iconName) {
  switch(iconName) {
    case 'add_alert':{
      return Icons.add_alert;
    }
     case 'accessibility':{
      return Icons.accessibility;
    }
     case 'folder_open':{
      return Icons.folder_open;
    }
     case 'data_usage':{
      return Icons.data_usage;
    }
     case 'input':{
      return Icons.input;
    }
     case 'tune':{
      return Icons.tune;
    }
     case 'list':{
      return Icons.list;
    }
  }
}

class HomePageTemp extends StatelessWidget {
  final options = [
    {'title':'Alertas','icon':'add_alert'},
    {'title':'Avatars','icon':'accessibility'},
    {'title':'Card-Tarjetas','icon':'folder_open'},
    {'title':'Animaciones','icon':'data_usage'},
    {'title':'Inputs','icon':'input'},
    {'title':'Sliders','icon':'tune'},
    {'title':'Listas y Scroll','icon':'list'}
    ];

  TextStyle styleText = TextStyle(color: Colors.red, fontSize: 25.0);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFC5CAE9),
      appBar: AppBar(
        title: Text('Componentes',style:TextStyle(fontSize: 28)),
        actions: <Widget>[
            Image.network(
                'https://www.masgamers.com/wp-content/uploads/2016/01/Logo_657747.png',
                  )
          ],
        backgroundColor: Color(0xFF3F51B5),
      ),
      body: ListView(
        children: _crearItemsCortal()
      ),
    );
  }
  
  List<Widget> _crearItemsCortal() {
    return options.map ((item) {
      return Column(
        children: <Widget>[
          ListTile(
            leading: Icon(getIcon(item['icon']),color: Color(0xFF3F51B5),size: 30),
            title: Text(item['title'], style:TextStyle(color: Color(0xFF212121))), 
            trailing: Icon(Icons.arrow_forward_ios,color: Color(0xFFFF5722)),
            onTap: (){
                FlutterToast.showToast(  
                    msg: "Estas en: "+item['title'],  
                    toastLength: Toast.LENGTH_SHORT,  
                    gravity: ToastGravity.BOTTOM,  
                    timeInSecForIosWeb: 2,  
                    backgroundColor: Color(0xFFFF5722),  
                    textColor: Colors.white,  
                    fontSize: 16.0  
                );  
              },
            ), 
          Divider(color: Color(0xFFBDBDBD),thickness: 3)
        ],
      );
    }).toList();
  }
}







/*body: Align(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Uno', style: styleText),
            Text('Dos', style: styleText),
            Text('Tress', style: styleText),
          ],
        ),
      )*/

 /*body: Center(
        child: Row(
          mainAxisAlignment:MainAxisAlignment.center,
          children: [
            FlutterLogo(
              size: 60.0,
            ),
            FlutterLogo(
              size: 60.0,
            ),
            FlutterLogo(
              size: 60.0,
            ),
          ]
        )
      )*/
/* body: Center(
        child: Row(
          mainAxisAlignment:MainAxisAlignment.center,
          children: [
            Icon(Icons.adb, color: Colors.red, size: 50.0),
            Icon(
              Icons.beach_access,
              color:Colors.blue,
              size: 36.0,
            )
          ],
        ),
      ),*/
/*body: Center(
        child: Row(
          mainAxisAlignment:MainAxisAlignment.center,
          children: [
            Image.asset('assets/images/imagen1.png'),
            Icon(
              Icons.beach_access,
              color: Colors.blue,
              size: 36.0,
            ),
          ],
        ),
      ),*/